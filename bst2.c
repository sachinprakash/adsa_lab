#include<stdio.h> 
#include<stdlib.h> 


int max(int a, int b){ return a>b? a: b;}
struct node 
{ 
	int key; 
	struct node *left, *right; 
}; 

struct node *newNode(int item) 
{ 
	struct node *temp = (struct node *)malloc(sizeof(struct node)); 
	temp->key = item; 
	temp->left = temp->right = NULL; 
	return temp; 
} 

struct node * minValueNode(struct node* node) 
{ 
    struct node* current = node; 
    while (current && current->left != NULL) 
        current = current->left; 
    return current; 
} 

void printPostorder(struct node* node) 
{ 
     if (node == NULL) 
        return; 
     printPostorder(node->left); 
     printPostorder(node->right); 
     printf("%d ", node->key); 
} 
   
void printPreorder(struct node* node) 
{ 
     if (node == NULL) 
          return; 
     printf("%d ", node->key);   
     printPreorder(node->left);   
     printPreorder(node->right); 
}     

int maxDepth(struct node* node)  
{  
    if (node == NULL)  
        return 0;  
    else
    {  
        int lDepth = maxDepth(node->left);  
        int rDepth = maxDepth(node->right);  
        if (lDepth > rDepth)  
            return(lDepth + 1);  
        else return(rDepth + 1);  
    }  
} 

struct node* deleteNode(struct node* root, int key) 
{ 
    if (root == NULL) return root; 
    if (key < root->key) 
        root->left = deleteNode(root->left, key); 
    else if (key > root->key) 
        root->right = deleteNode(root->right, key); 
    else
    { 
        if (root->left == NULL) 
        { 
            struct node *temp = root->right; 
            free(root); 
            return temp; 
        } 
        else if (root->right == NULL) 
        { 
            struct node *temp = root->left; 
            free(root); 
            return temp; 
        } 
        struct node* temp = minValueNode(root->right); 
        root->key = temp->key; 
        root->right = deleteNode(root->right, temp->key); 
    } 
    return root; 
} 

void inorder(struct node *root) 
{ 
	if (root != NULL) 
	{ 
		inorder(root->left); 
		printf("%d ", root->key); 
		inorder(root->right); 
	} 
} 

struct node* insert(struct node* node, int key) 
{ 
	if (node == NULL) return newNode(key); 
	if (key < node->key) 
		node->left = insert(node->left, key); 
	else if (key > node->key) 
		node->right = insert(node->right, key); 
	return node; 
} 

int maxpath(struct node* root, int* res){
	int max_single,max_top;
	if(root ==NULL){
		return 0;
	}
	
	int l = maxpath(root->left,res); 
    int r = maxpath(root->right,res);
    max_single = max(max(l, r) + root->key, root->key);
    max_top = max(max_single, l + r + root->key);
    *res = max(*res, max_top);
    return max_single; 
}

int main() 
{ 
	int sum=0;
	int temp, choice;
	struct node *root = NULL; 
	printf("enter root element: ");
	sum=0;
	scanf("%d", &temp);
	root = insert(root, temp); 
	choice=-1;
	while(choice!=0){
		printf("\n1. insert\n2. inorder traversal\n3. delete\n4. height\n5. preorder traversal\n6. postorder traversal\n7. max path\n0. exit: ");
		printf("\nEnter the choice: ");
		scanf("%d", &choice);
		switch(choice){
			case 1: printf("\nEnter the element: ");
					scanf("%d", &temp);
					insert(root, temp);
					break;
			case 2: inorder(root); break;
			case 3: printf("\nEnter the key to delete: "); scanf("%d", &temp);
				deleteNode(root, temp); inorder(root); break;
			case 4: printf("\nHeight is %d", maxDepth(root)); break;
			case 5: printPreorder(root); break;
			case 6: printPostorder(root); break;
			case 7: maxpath(root, &sum); printf("max path value is %d", sum); break;
			default: exit(0);
		}
	}
	return 0; 
} 

